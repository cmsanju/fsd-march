package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCalculator {
	
	Calculator obj;
	
	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("Before Class");
	}
	
	@AfterClass
	public static void afterClass()
	{
		System.out.println("After Class");
	}
	
	@Before
	public void setUp()
	{
		System.out.println("Before test method");
		
		obj = new Calculator();
	}
	
	@After
	public void setDown()
	{
		System.out.println("After test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("add test method");
		
		assertEquals(40, obj.add(10, 30));
	}
	
	@Test
	public void testSub()
	{
		System.out.println("sub test method");
		
		assertEquals(20, obj.sub(50, 20));
	}
	
	@Test
	public void testMul()
	{
		System.out.println("mul test method");
		
		assertEquals(100, obj.mul(50, 2));
	}
	
	@Test
	public void testGreetMsg()
	{
		System.out.println("greetMsg test method");
		
		assertEquals("hello hi", obj.greetMsg("hello hi"));
	}
}
